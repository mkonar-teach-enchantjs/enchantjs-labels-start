/** Labels **/
window.onload = myGame;
enchant();

function myGame() {
    // New game with a 320x320 pixel canavs, 24 frames/sec.
    var game = new Core(320, 320);
    game.fps = 24;
    
    // Specify what should happen when the game loads.
    game.onload = function () {
        game.rootScene.backgroundColor = "silver";

        // Make a label.
        var label = new Label("Hello, world.");
        
        game.rootScene.addChild(label);
    };
    
    // Start the game.
    game.start();
}
